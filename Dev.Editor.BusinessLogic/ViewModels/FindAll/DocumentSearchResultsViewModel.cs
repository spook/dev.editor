﻿using Dev.Editor.BusinessLogic.Models.SearchResults;
using Dev.Editor.BusinessLogic.ViewModels.BottomTools.SearchResults;
using Dev.Editor.BusinessLogic.ViewModels.FindInFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Dev.Editor.BusinessLogic.ViewModels.FindAll
{
    public class DocumentSearchResultsViewModel : BaseSearchResultRootViewModel
    {
        private bool isFiltered;

        public DocumentSearchResultsViewModel(string path, 
            Guid guid, 
            string searchPattern,
            ImageSource icon, 
            List<DocumentSearchResultViewModel> results)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Invalid path!", nameof(path));

            Path = path;
            SearchPattern = searchPattern;
            Icon = icon;
            Results = results;
            IsExpanded = true;
            IsFiltered = false;
        }

        public override void ApplyFilter(SearchResultFilterModel model)
        {
            string path = model.CaseSensitive ? Path : Path.ToLower();
            string updatedFilter = model.CaseSensitive ? model.Filter : model.Filter.ToLower();
            bool resultsVisible = path.Contains(updatedFilter) ^ model.FilterExcludes;

            foreach (var result in Results)
            {
                result.IsVisible = resultsVisible;
            }

            IsFiltered = true;
        }

        public override void ClearFilter()
        {
            foreach (var result in Results)
            {
                result.IsVisible = true;
            }

            IsFiltered = false;
        }

       
        public override bool IsFiltered
        {
            get => isFiltered;
            set => Set(ref isFiltered, () => IsFiltered, value);
        }

        public string Path { get; }
        public ImageSource Icon { get; }
        public List<DocumentSearchResultViewModel> Results { get; }
        public string SearchPattern { get; }
        public int Count => Results.Count;
        public string Display => $"{Path} ({Count})";
    }
}
