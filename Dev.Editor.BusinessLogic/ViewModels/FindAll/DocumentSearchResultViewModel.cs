﻿using Dev.Editor.BusinessLogic.ViewModels.BottomTools.SearchResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.Editor.BusinessLogic.ViewModels.FindAll
{
    public class DocumentSearchResultViewModel : BaseSearchResultViewModel
    {
        public DocumentSearchResultViewModel(Guid documentGuid,
            string before,
            string match,
            string exact,
            string after,
            int line,
            int column,
            int offset,
            int length)
        {
            DocumentGuid = documentGuid;
            Before = before;
            Match = match;
            Exact = exact;
            After = after;
            Line = line;
            Column = column;
            Offset = offset;
            Length = length;
        }

        public Guid DocumentGuid { get; }
        public string Before { get; }
        public string Match { get; }
        public string Exact { get; }
        public string After { get; }
        public int Line { get; }
        public int Column { get; }
        public int Offset { get; }
        public int Length { get; }

        public string Location => $"({Line}, {Column})";
    }
}
