﻿using Dev.Editor.BusinessLogic.ViewModels.BottomTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Dev.Editor.Resources;
using Dev.Editor.BusinessLogic.Services.ImageResources;
using Dev.Editor.BusinessLogic.ViewModels.Search;
using System.Collections.ObjectModel;
using Dev.Editor.BusinessLogic.ViewModels.FindInFiles;
using Spooksoft.VisualStateManager.Conditions;
using System.Windows.Input;
using Spooksoft.VisualStateManager.Commands;
using Dev.Editor.BusinessLogic.ViewModels.DuplicatedLines;
using Dev.Editor.BusinessLogic.Models.SearchResults;
using Dev.Editor.BusinessLogic.ViewModels.FindAll;
using Dev.Editor.BusinessLogic.Services.Messaging;

namespace Dev.Editor.BusinessLogic.ViewModels.BottomTools.SearchResults
{
    public class SearchResultsBottomToolViewModel : BaseBottomToolViewModel
    {
        private readonly ISearchResultsHandler searchResultsHandler;
        private readonly IImageResources imageResources;
        private readonly IMessagingService messagingService;
        private readonly ImageSource icon;

        private readonly BaseCondition resultsNonEmptyCondition;
        private readonly BaseCondition resultsAreReplaceCondition;
        private readonly BaseCondition resultsAreSearchCondition;
        private readonly BaseCondition resultsCanFilterContents;

        private BaseSearchResultRootViewModel searchResults;
        private string filter;
        private bool filterCaseSensitive;
        private bool filterExcludes;
        private bool filterFiles;
        private bool filterContents;

        private void DoClearSearchResults()
        {
            searchResults = null;
            OnPropertyChanged(() => SearchResults);
        }

        private void DoPerformReplace()
        {
            searchResultsHandler.PerformReplaceInFiles((ReplaceResultsViewModel)searchResults);
            searchResults = null;
            OnPropertyChanged(() => SearchResults);
        }

        private void HandleFilterChanged()
        {
            if (searchResults != null)
            {
                if (!string.IsNullOrEmpty(filter))
                    searchResults.ApplyFilter(new SearchResultFilterModel(filter, filterCaseSensitive, filterExcludes, filterFiles, filterContents));
                else
                    searchResults.ClearFilter();
            }
        }

        private void CollectResultsRecursive(BaseSearchResultViewModel result, StringBuilder sb)
        {
            switch (result)
            {
                case DocumentSearchResultsViewModel documentSearchResults:
                    {
                        foreach (var item in documentSearchResults.Results.Where(item => item.IsVisible))
                            CollectResultsRecursive(item, sb);
                        break;
                    }
                case DocumentSearchResultViewModel documentSearchResult:
                    {
                        if (documentSearchResult.IsVisible)
                            sb.AppendLine(documentSearchResult.Exact);

                        break;
                    }
                case DuplicatedContentPreviewViewModel duplicatedContent:
                case DuplicatedLineCaseViewModel duplicatedLineCase:
                case DuplicatedLinesResultViewModel duplicatedLines:
                case FileReferenceViewModel fileReference:
                    {
                        break;
                    }
                case FileSearchResultViewModel fileSearchResult:
                    {
                        foreach (var item in fileSearchResult.Results.Where(item => item.IsVisible))
                            CollectResultsRecursive(item, sb);
                        break;
                    }
                case FolderSearchResultViewModel folderSearchResult:
                    {
                        foreach (var item in folderSearchResult.Files.Where(item => item.IsVisible))
                            CollectResultsRecursive(item, sb);
                        break;
                    }
                case ReplaceResultsViewModel replaceResults:
                    {
                        foreach (var item in replaceResults.Results.Where(item => item.IsVisible))
                            CollectResultsRecursive(item, sb);
                        break;
                    }
                case ReplaceResultViewModel replaceResult:
                    {
                        if (replaceResult.IsVisible)
                            sb.AppendLine(replaceResult.Match);
                        break;
                    }
                case SearchResultsViewModel searchResults:
                    {
                        foreach (var item in searchResults.Results.Where(item => item.IsVisible))
                            CollectResultsRecursive(item, sb);
                        break;
                    }
                case SearchResultViewModel searchResult:
                    {
                        if (searchResult.IsVisible)
                            sb.AppendLine(searchResult.Match);
                        break;
                    }
            }
        }

        private void DoExportResultsToDocument()
        {
            StringBuilder sb = new StringBuilder();
            CollectResultsRecursive(searchResults, sb);

            searchResultsHandler.CreateNewDocument(sb.ToString());
        }        

        public SearchResultsBottomToolViewModel(ISearchResultsHandler searchResultsHandler, 
            IImageResources imageResources,
            IMessagingService messagingService)
            : base(searchResultsHandler)
        {
            this.searchResultsHandler = searchResultsHandler;
            this.imageResources = imageResources;
            this.messagingService = messagingService;

            filter = null;
            filterCaseSensitive = false;
            filterExcludes = false;
            filterFiles = true;
            filterContents = true;

            searchResults = null;
            icon = imageResources.GetIconByName("Search16.png");

            resultsNonEmptyCondition = new ChainedLambdaCondition<SearchResultsBottomToolViewModel>(this, vm => vm.SearchResults.Single() != null, false);
            resultsAreReplaceCondition = new ChainedLambdaCondition<SearchResultsBottomToolViewModel>(this, vm => vm.SearchResults.SingleOrDefault() is ReplaceResultsViewModel, false);
            resultsAreSearchCondition = new ChainedLambdaCondition<SearchResultsBottomToolViewModel>(this, vm => vm.SearchResults.SingleOrDefault() is SearchResultsViewModel, false);

            resultsCanFilterContents = new ChainedLambdaCondition<SearchResultsBottomToolViewModel>(this, vm => vm.SearchResults.SingleOrDefault() is DuplicatedLinesResultViewModel, false);
            resultsCanFilterContents.PropertyChanged += (s, e) => OnPropertyChanged(() => CanFilterContents);

            ClearSearchResultsCommand = new AppCommand(obj => DoClearSearchResults(), resultsNonEmptyCondition);
            PerformReplaceCommand = new AppCommand(obj => DoPerformReplace(), resultsAreReplaceCondition);
            ExportResultsToDocumentCommand = new AppCommand(obj => DoExportResultsToDocument(), resultsNonEmptyCondition);
        }

        public void NotifyItemDoubleClicked(object selectedResult)
        {
            if (selectedResult is SearchResultViewModel searchResult)
            {
                searchResultsHandler.OpenFileSearchResult(searchResult.FullPath, searchResult.Line, searchResult.Column, searchResult.Length);
            }
            else if (selectedResult is FileReferenceViewModel fileRef)
            {
                searchResultsHandler.OpenFileSearchResult(fileRef.Path, fileRef.StartLine, 0, 0);
            }
            else if (selectedResult is DocumentSearchResultViewModel documentRes) 
            {
                searchResultsHandler.OpenDocumentSearchResult(documentRes.DocumentGuid, documentRes.Line, documentRes.Column, documentRes.Length);
            }
        }

        public void SetResults(BaseSearchResultRootViewModel results)
        {
            // Setting search results to null to prevent Filter to fire
            searchResults = null;
            Filter = null;

            searchResults = results;
            OnPropertyChanged(() => SearchResults);
        }

        public override string Title => Strings.BottomTool_SearchResults_Title;

        public override ImageSource Icon => icon;

        public override string Uid => SearchResultsUid;

        public IEnumerable<BaseSearchResultRootViewModel> SearchResults
        {
            get 
            {
                yield return searchResults;
            }
        }

        public string Filter
        {
            get => filter;
            set => Set(ref filter, () => Filter, value, () => HandleFilterChanged());
        }

        public bool FilterCaseSensitive
        {
            get => filterCaseSensitive;
            set => Set(ref filterCaseSensitive, () => FilterCaseSensitive, value, () => HandleFilterChanged());
        }

        public bool FilterExcludes
        {
            get => filterExcludes;
            set => Set(ref filterExcludes, () => FilterExcludes, value, () => HandleFilterChanged());
        }

        public bool FilterFiles
        {
            get => filterFiles;
            set => Set(ref filterFiles, () => FilterFiles, value, () => HandleFilterChanged());
        }

        public bool FilterContents
        {
            get => filterContents;
            set => Set(ref filterContents, () => FilterContents, value, () => HandleFilterChanged());
        }

        public ICommand ClearSearchResultsCommand { get; }

        public ICommand PerformReplaceCommand { get; }

        public ICommand ExportResultsToDocumentCommand { get; }

        public bool CanFilterContents => resultsCanFilterContents.Value;
    }
}
