﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Dev.Editor.BusinessLogic.ViewModels.Tools.Find
{
    public static class FindResultFormatter
    {
        // Private constants --------------------------------------------------

        private const int CHARS_BEFORE_AFTER = 80;
        private const int MAX_CONTENT_LENGTH = 100;

        // Public methods -----------------------------------------------------

        public static string EvalMatchContent(string text, Match match)
        {
            string matchContent;
            if (match.Length < MAX_CONTENT_LENGTH)
                matchContent = text.Substring(match.Index, match.Length);
            else
                matchContent = text.Substring(match.Index, MAX_CONTENT_LENGTH / 2) +
                    " ... " +
                    text.Substring(match.Index + match.Length - MAX_CONTENT_LENGTH / 2, MAX_CONTENT_LENGTH / 2);
            return matchContent;
        }

        public static string EvalAfter(string text, Match match)
        {
            var after = text.Substring(match.Index + match.Length, Math.Min(CHARS_BEFORE_AFTER, text.Length - (match.Index + match.Length)));
            int firstLineFeed = after.IndexOf('\n');
            int firstLineFeedCarriageReturn = after.IndexOf("\r\n");
            int firstLineBreakInAfter = Math.Min(firstLineFeed >= 0 ? firstLineFeed : -1, firstLineFeedCarriageReturn >= 0 ? firstLineFeedCarriageReturn : -1);

            if (firstLineBreakInAfter >= 0)
                after = after.Substring(0, firstLineBreakInAfter);
            else
                after = after + "...";

            return after;
        }

        public static string EvalBefore(string text, Match match)
        {
            var before = text.Substring(Math.Max(0, match.Index - CHARS_BEFORE_AFTER), Math.Min(match.Index, CHARS_BEFORE_AFTER));
            int lastLineFeed = before.LastIndexOf('\n');
            int lastLineFeedCarriageReturn = before.LastIndexOf("\r\n");
            int lastLineBreakInBefore = Math.Max(lastLineFeed >= 0 ? lastLineFeed + 1 : -1, lastLineFeedCarriageReturn >= 0 ? lastLineFeedCarriageReturn + 2 : -1);

            if (lastLineBreakInBefore >= 0)
                before = before.Substring(lastLineBreakInBefore);
            else
                before = "..." + before;
            return before;
        }
    }
}
