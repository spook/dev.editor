﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.Editor.BinAnalyzer.AnalyzerDefinition
{
    public interface IAnalyzerContext
    {
        void Log(int line, int column, long offset, string message);
    }
}
