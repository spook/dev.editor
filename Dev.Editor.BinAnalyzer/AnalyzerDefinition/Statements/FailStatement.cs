﻿using Dev.Editor.BinAnalyzer.Data;
using Dev.Editor.BinAnalyzer.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.Editor.BinAnalyzer.AnalyzerDefinition.Statements
{
    class FailStatement : BaseStatement
    {
        private string message;

        public FailStatement(int line, int column, string message) 
            : base(line, column)
        {
            this.message = message;
        }

        internal override void Read(BinaryReader reader, List<BaseData> result, Scope scope, IAnalyzerContext context)
        {
            throw new AnalysisException(Line, Column, reader.BaseStream.Position, message, string.Format(Resources.Strings.Message_AnalysisError_UserError, message));
        }
    }
}
