﻿using Dev.Editor.BinAnalyzer.Data;
using Dev.Editor.BinAnalyzer.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.Editor.BinAnalyzer.AnalyzerDefinition.Statements
{
    internal class LogStatement : BaseStatement
    {
        private string message;

        public LogStatement(int line, int column, string message)
            : base(line, column)
        {
            this.message = message;
        }

        internal override void Read(BinaryReader reader, List<BaseData> result, Scope scope, IAnalyzerContext context)
        {
            context?.Log(Line, Column, reader.BaseStream.Position, $"({Line + 1},{Column + 1} @ 0x{reader.BaseStream.Position:X}): {message}");
        }
    }
}
