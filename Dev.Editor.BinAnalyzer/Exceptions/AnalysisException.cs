﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.Editor.BinAnalyzer.Exceptions
{
    public class AnalysisException : BaseSourceReferenceException
    {
        public AnalysisException(int line, int column, long offset, string message, string localizedMessage) 
            : base(line, column, message, localizedMessage)
        {
            Offset = offset;
        }

        public long Offset { get; }

        public override string LocalizedErrorMessage => $"({Line + 1},{Column + 1} @ 0x{Offset:X}): {base.LocalizedErrorMessage}";
    }
}
